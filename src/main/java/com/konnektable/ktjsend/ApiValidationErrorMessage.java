package com.konnektable.ktjsend;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

/**
 * Class for representing validation errors. Instantiate the class through the builder.
 *
 * @author Spyros Vallianos
 */
@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ApiValidationErrorMessage {

  private final String field;
  private final Object rejectedValue;
  private final String message;
}
