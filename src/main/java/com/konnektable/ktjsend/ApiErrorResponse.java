package com.konnektable.ktjsend;

import static java.util.Objects.requireNonNullElseGet;
import static org.springframework.util.Assert.notNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * Class for creating a JSend error response. Instantiate the class through the builder.
 *
 * @author Spyros Vallianos
 */
@ToString
@EqualsAndHashCode
@Getter
public final class ApiErrorResponse {

  private final String status;

  private final String code;

  private List<Object> data;

  private final String message;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
  private final LocalDateTime timestamp = LocalDateTime.now();

  @Builder
  private ApiErrorResponse(HttpStatus code, String message, List<Object> data) {

    notNull(code, "The http code cannot be null.");
    notNull(message, "The message cannot be null.");

    this.code = code.toString();
    this.status = resolveStatus(code);
    this.message = message;
    this.data = requireNonNullElseGet(data, ArrayList::new);
  }

  public void addData(final Object subError) {
    this.data.add(subError);
  }

  private String resolveStatus(HttpStatus httpStatusCode) {
    return httpStatusCode.is4xxClientError() ? Status.FAIL.getValue() : Status.ERROR.getValue();
  }
}
