package com.konnektable.ktjsend;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Class for JSend Enum Status representation.
 *
 * @author Spyros Vallianos
 */
@Getter
@RequiredArgsConstructor
enum Status {
  SUCCESS("success"),
  FAIL("fail"),
  ERROR("error");

  private final String value;

  @Override
  public String toString() {
    return this.getValue();
  }
}
