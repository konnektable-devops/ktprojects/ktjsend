package com.konnektable.ktjsend;

import static org.springframework.util.Assert.notNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * Class for creating a JSend success response. Instantiate the class through the builder.
 *
 * @author Spyros Vallianos
 */
@ToString
@EqualsAndHashCode
@Getter
public final class ApiSuccessResponse {

  private final String status = Status.SUCCESS.getValue();

  private final String code;

  private Object data;

  private final String message;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm", timezone = "UTC")
  private final LocalDateTime timestamp = LocalDateTime.now();

  @Builder
  private ApiSuccessResponse(HttpStatus code, Object data, String message) {

    notNull(code, "The http code cannot be null.");
    notNull(message, "The message cannot be null.");

    this.code = code.toString();
    this.data = data;
    this.message = message;
  }
}
